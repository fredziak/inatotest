# Inato Test

## Context

Developped for Inato by Frédéric ESNAULT from [Inato Test instructions](https://github.com/inato/senior-take-home-test)

## Inato Test API

backend code in inato-test-api/

## Inato Test CLI

CLI application in inato-test-cli/