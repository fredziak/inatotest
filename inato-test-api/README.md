# Inato Test API

## Context

Developped for Inato by Frédéric ESNAULT from [Inato Test instructions](https://github.com/inato/senior-take-home-test)

## Technical choices

* Java
* Maven for dependency management
* Spring Boot 
* REST API thru Spring Web
* Jayway JsonPath (implementation of the JSONPath specification)

## Build, run and test 

### Direct run from development environment

Server can be built and started typing:

```
mvn spring-boot:run
```

Prerequisites : 
* Java JDK 1.8+
* Maven 3

Then the API can be called thanks to the following API endpoints.

### Package

The API runnable package is produced in target/inato-test-api-1.0.0.jar with:
```
mvn spring-boot:run
```

Prerequisites : 
* Java JDK 1.8+
* Maven 3

### Run from the package

The package can be run with this command:
```
java -jar inato-test-api-1.0.0.jar
```

Prerequisites : 
* Java JRE 8+

## API endpoints

* http://localhost:8080/test : dummy test to check server state
* http://localhost:8080/trials?sponsor=Sanofi : returns trial data for the given sponsor
* http://localhost:8080/trialsWithCountryName?country=FR : returns trial name and country name for the given country
* http://localhost:8080/countries : return raw countries data

## Implementation notes

* Project skeleton initially created using [Spring Initializer](https://start.spring.io/)
* Spring Web REST Controller to expose API
* Data files are included in classpath for a start
* JsonPath is used to manage JSON data (filtering functionality)
* Work on data is mainly done by TrialService and tested by TrialServiceTest
* Country data is managed the same way

### Possible improvements

* Keep data in a memory cache if they do not evolve frequently
* Support mixed case in country codes
* More unit test cases and asserts

## References

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Jayway JsonPath](https://github.com/json-path/JsonPath)
* [Introduction to JsonPath](https://www.baeldung.com/guide-to-jayway-jsonpath)
