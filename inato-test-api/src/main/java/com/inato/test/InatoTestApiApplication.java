package com.inato.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InatoTestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InatoTestApiApplication.class, args);
	}

}
