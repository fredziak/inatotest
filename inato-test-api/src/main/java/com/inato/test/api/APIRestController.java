package com.inato.test.api;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inato.test.service.CountryService;
import com.inato.test.service.TrialService;

/**
 * Main Rest controller to expose the API
 * 
 * @author fesnault
 */
@RestController
public class APIRestController {

	@Autowired
	private TrialService trialService;

	@Autowired
	private CountryService countryService;

	/**
	 * Dummy test endpoint
	 * 
	 * @return a comment if OK
	 */
	@RequestMapping("/test")
	public String test() {
		return "Inato API is alive";
	}

	/**
	 * Return ongoing trial data for a given sponsor
	 * 
	 * @param sponsor
	 * @return trial data (name, dates and sponsor only)
	 */
	@RequestMapping("/trials")
	public List<Map<String, Object>> trialsBySponsor(@RequestParam String sponsor) {
		return trialService.getOngoingTrialsBySponsor(sponsor);
	}

	/**
	 * Return ongoing trial data for a given country
	 * 
	 * @param countryCode
	 * @return trial data (name and country name only)
	 */
	@RequestMapping("/trialsWithCountryName")
	public List<Map<String, Object>> trialsWithCountryName(@RequestParam String countryCode) {
		// get raw trial data
		List<Map<String, Object>> data = trialService.getOngoingTrialsByCountry(countryCode);
		// then get country name
		String countryName = countryService.getCountryNameForCode(countryCode);
		// replace country code by country name
		for (Map<String, Object> item : data) {
			item.put("country", countryName);
		}
		return data;
	}

	/**
	 * Return countries data
	 * 
	 * @return countries data (raw JSON)
	 */
	@RequestMapping("/countries")
	public List<Map<String, Object>> countries() {
		return countryService.getCountries();
	}
}