package com.inato.test.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.jayway.jsonpath.Criteria;
import com.jayway.jsonpath.JsonPath;

/**
 * Service to manage trials data
 * 
 * @author fesnault
 */
@Service
public class CountryService {
	private static final String FILENAME = "/countries.json";
			
	/**
	 * Return all countries data
	 * @return found countries
	 */
	public List<Map<String, Object>> getCountries() {
		InputStream is = this.getClass().getResourceAsStream(FILENAME);
		return JsonPath.parse(is).read("$");
	}
	
	/**
	 * Return country name for the given code
	 * @param country code
	 * @return country name
	 */
	public String getCountryNameForCode(String code) {
		InputStream is = this.getClass().getResourceAsStream(FILENAME);
		List<String> data = JsonPath.parse(is).read("$[?]['name']", Criteria.where("code").eq(code.toUpperCase()));
		if (data == null || data.isEmpty()) {
			return null;
		}
		return data.get(0);
	}
}
