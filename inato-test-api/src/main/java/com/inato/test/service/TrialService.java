package com.inato.test.service;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.jayway.jsonpath.Criteria;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Predicate;

/**
 * Service to manage trials data
 * 
 * @author fesnault
 */
@Service
public class TrialService {
	private static final String FILENAME = "/trials.json";
	
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
			
	/**
	 * Retrieve all trials for a given sponsor
	 * @param sponsor
	 * @return found trials
	 */
	public List<Map<String, Object>> getTrialsBySponsor(String sponsor) {
		InputStream is = this.getClass().getResourceAsStream(FILENAME);
		return JsonPath.parse(is).read("$[?]", Criteria.where("sponsor").eq(sponsor));
	}
	
	/**
	 * Retrieve ongoing trials for a given sponsor. 
	 * @param sponsor
	 * @return found trials (only name, dates and sponsor fields are returned)
	 */
	public List<Map<String, Object>> getOngoingTrialsBySponsor(String sponsor) {
		InputStream is = this.getClass().getResourceAsStream(FILENAME);
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(Criteria.where("sponsor").eq(sponsor));
		predicates.addAll(ongoingPredicates());
		return JsonPath.parse(is).read("$[?]['name', 'start_date', 'end_date', 'sponsor']", Filter.filter(predicates));
	}
	
	/**
	 * Retrieve ongoing trials for a given country code. 
	 * 
	 * @param countryCode
	 * @return found trials (name and country are returned)
	 */
	public List<Map<String, Object>> getOngoingTrialsByCountry(String countryCode) {
		InputStream is = this.getClass().getResourceAsStream(FILENAME);
		List<Predicate> predicates = new ArrayList<Predicate>();
		List<String> countryCodeVariants = new ArrayList<String>();
		countryCodeVariants.add(countryCode.toLowerCase());
		countryCodeVariants.add(countryCode.toUpperCase());
		predicates.add(Criteria.where("country").in(countryCodeVariants));
		predicates.addAll(ongoingPredicates());
		return JsonPath.parse(is).read("$[?]['name', 'country']", Filter.filter(predicates));
	}
	
	/* 
	 * describe conditions for an ongoing trial. 
	 * A trial is ongoing if: its start date is in the past, its end date is in the future, it has not been canceled
	 * @return list of predicates
	 */
	private List<Predicate> ongoingPredicates() {
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(Criteria.where("canceled").eq(false));
		String now = DATE_FORMAT.format(new Date());
		predicates.add(Criteria.where("start_date").lt(now));
		predicates.add(Criteria.where("end_date").gt(now));
		return predicates;
	}
}
