/**
 * 
 */
package com.inato.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * CountryService Test. 
 * @author fesnault
 *
 */
public class CountryServiceTest {

	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCountries() {
		CountryService service = new CountryService();
		List<Map<String, Object>> result = service.getCountries();
		System.out.println(result);
		
		assertNotNull(result);
		assertEquals(5, result.size());
	}
	
	@Test
	public void testCountryNameForCode() {
		CountryService service = new CountryService();
		String result = service.getCountryNameForCode("ES");
		System.out.println(result);
		
		assertNotNull(result);
		assertEquals("Spain", result);
	}
}
