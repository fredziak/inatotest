/**
 * 
 */
package com.inato.test.service;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * TrialService Test. Based on data in
 * 
 * @author fesnault
 *
 */
public class TrialServiceTest {

	private String now;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		now = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTrialsBySponsor() {
		String sponsor = "Sanofi";
		TrialService service = new TrialService();
		List<Map<String, Object>> result = service.getTrialsBySponsor(sponsor);
		System.out.println(result);

		assertNotNull(result);
		assertEquals(6, result.size());

		// check sponsor in results
		for (Map<String, Object> trial : result) {
			System.out.println(trial);
			assertEquals(sponsor, trial.get("sponsor"));
		}
	}

	@Test
	public void testOngoingTrialsBySponsor() {
		String sponsor = "Sanofi";
		TrialService service = new TrialService();
		List<Map<String, Object>> result = service.getOngoingTrialsBySponsor(sponsor);
		System.out.println(result);
		assertNotNull(result);
		assertEquals(2, result.size());

		// check results
		for (Map<String, Object> trial : result) {
			System.out.println(trial);
			assertEquals(sponsor, trial.get("sponsor")); // sponsor must be the
															// given one
			checkOngoing(trial);
			// these field should not be present:
			assertNull(trial.get("canceled"));
			assertNull(trial.get("study_type"));
			assertNull(trial.get("primary_purpose"));
			assertNull(trial.get("country"));
		}
	}

	/**
	 * @param trial
	 */
	private void checkOngoing(Map<String, Object> trial) {
		assertNotEquals("Canceled trial", trial.get("name")); // trial should
																// not be
																// canceled
		// start date is in the past and end date is in the future
		String start = (String) trial.get("start_date");
		assertNotNull(start);
		String end = (String) trial.get("end_date");
		assertNotNull(end);
		assertTrue(start.compareTo(now) < 0);
		assertTrue(end.compareTo(now) > 0);
	}

	@Test
	public void testOngoingTrialsByCountry() {
		String countryCode = "FR";
		TrialService service = new TrialService();
		List<Map<String, Object>> result = service.getOngoingTrialsByCountry(countryCode);
		System.out.println(result);
		assertNotNull(result);
		assertEquals(2, result.size());

		// check results
		for (Map<String, Object> trial : result) {
			System.out.println(trial);
			assertTrue(countryCode.equalsIgnoreCase((String) trial.get("country"))); 
		}
	}

}
