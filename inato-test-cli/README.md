# Inato Test CLI

## Context

Developped for Inato by Frédéric ESNAULT from [Inato Test instructions](https://github.com/inato/senior-take-home-test)

## Technical choices

* Java
* Maven for dependency management
* Apache Commons HTTP Client 
* Jayway JsonPath

## Build

The application can be built typing (from inato-test-api/):

```
mvn package
```

Prerequisites : 
* Java JDK 1.8+
* Maven 3

As a result, everything is packaged into target/inato-test-cli-1.0.0-jar-with-dependencies.jar

## Run and test

Prerequisites : 
* Java JRE 8+
* inato-test-cli-1.0.0-jar-with-dependencies.jar

To run the CLI application:

```
java -jar inato-test-cli-1.0.0-jar-with-dependencies.jar [countryCode]
```

For instance:

```
java -jar inato-test-cli-1.0.0-jar-with-dependencies.jar FR
```

### In development environment

You can also run the com.inato.test.cli.InatoTestCli class directly

## Implementation notes

* Project skeleton created thru basic Maven configuration
* CLI is a simple Java class with a main method
* Main work on data (filtering and getting country name) is done server side by the trialsWithCountryName endpoint
* A specific Http Client class has been designed: 
	* Apache HTTP Client lib is used to request the API
	* it relies upon implementations of a ResponseHandler interface that use Jayway JsonPath to manage JSON data
	
### Possible improvements

* API URL is hard coded -> provide a way to configure it
* Implement some automated tests

## References

* [HTTP Client quickstart](https://hc.apache.org/httpcomponents-client-ga/quickstart.html)
* [Jayway JsonPath](https://github.com/json-path/JsonPath)
* [Introduction to JsonPath](https://www.baeldung.com/guide-to-jayway-jsonpath)
