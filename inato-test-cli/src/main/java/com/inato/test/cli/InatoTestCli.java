/**
 * 
 */
package com.inato.test.cli;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.inato.test.cli.handlers.ErrorHandler;
import com.inato.test.cli.handlers.TrialsHandler;
import com.inato.test.cli.http.HttpClient;

/**
 * Inato Test CLI Application
 * 
 * @author fesnault
 */
public class InatoTestCli {

	private static final String BASE_URI = "http://localhost:8080";
	
	// input args
	private static String countryCode;
	
	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		checkArgs(args);
		try {
			HttpClient httpClient = new HttpClient(BASE_URI);
			TrialsHandler trialHandler = new TrialsHandler();
			ErrorHandler errorHandler = new ErrorHandler();
			Map<String, String> params = new HashMap<String, String>();
			params.put("countryCode", countryCode);
			boolean ok = httpClient.makeHttpRequest("/trialsWithCountryName", params, trialHandler, errorHandler);
			if (ok) {
				List<String> output = trialHandler.getTrialsAndCountryNames();
				for (String trialToDisplay : output) {
					System.out.println(trialToDisplay);
				}
				if (output.isEmpty()) {
					System.out.println("No data found");
				}
			} else {
				// got error from server
				System.err.println("ERROR: " + errorHandler.getErrorData());
				System.exit(-1);
			}
		} catch (Exception e) {
			System.err.println("UNEXPECTED ERROR: " + e.getMessage());
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/** Get and check input arguments 
	 * @param args
	 */
	private static void checkArgs(String[] args) {
		if (args == null || args.length == 0 || args[0].equalsIgnoreCase("--help")) {
			System.out.println("USAGE: java -jar inato-test-cli-1.0.0-jar-with-dependencies.jar <countryCode>");
			System.exit(-1);
		}
		countryCode =  args[0];
	}

}
