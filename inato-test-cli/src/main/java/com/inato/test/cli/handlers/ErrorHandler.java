/**
 * 
 */
package com.inato.test.cli.handlers;

import java.io.InputStream;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

/**
 * Read trials data
 * 
 * @author fesnault
 *
 */
public class ErrorHandler implements ResponseHandler {

	private DocumentContext docContext;

	@Override
	public void handleReponse(InputStream is) throws Exception {
		docContext = JsonPath.parse(is);
	}

	/**
	 * Return error data
	 * 
	 * @return map of error fields
	 */
	public Map<String, Object> getErrorData() {
		return docContext.read("$");
	}
}
