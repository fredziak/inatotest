package com.inato.test.cli.handlers;

import java.io.InputStream;

/**
 * Interface for Response Handlers
 * 
 * @author fesnault
 *
 */
public interface ResponseHandler {
	/**
	 * handleResponse
	 * 
	 * @param is reponse input stream
	 * @throws Exception
	 */
	void handleReponse(InputStream is) throws Exception;
}
