/**
 * 
 */
package com.inato.test.cli.handlers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

/**
 * Read trials data
 * 
 * @author fesnault
 *
 */
public class TrialsHandler implements ResponseHandler {

	private DocumentContext docContext;

	@Override
	public void handleReponse(InputStream is) throws Exception {
		docContext = JsonPath.parse(is);		
	}

	/** Return trials and country names to be displayed
	 * @return list of string with names separated by a comma
	 */
	public List<String> getTrialsAndCountryNames() {
		List<Map<String, String>> data = docContext.read("$");
		List<String> output = new ArrayList<String>(data.size());
		for (Map<String, String> trial : data) {
			output.add(trial.get("name") + ", " + trial.get("country"));
		}
		return output;		
	}
}
