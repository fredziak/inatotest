/**
 * 
 */
package com.inato.test.cli.http;

import java.net.URI;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.inato.test.cli.handlers.ErrorHandler;
import com.inato.test.cli.handlers.ResponseHandler;

/**
 * Http Client Wrapper using ResponseHandlers
 * @see ResponseHandler
 * @author fesnault
 *
 */
public class HttpClient {
	private String baseUrl;

	public HttpClient(String baseUrl) {
		super();
		this.baseUrl = baseUrl;
	}
	
	/**
	 * Execute the given HTTP Request
	 * 
	 * @param baseUrl
	 * @param path
	 * @param params
	 * @return boolean if no error occured
	 * @throws Exception
	 */
	public boolean makeHttpRequest(String path, Map<String, String> params, ResponseHandler handler, ResponseHandler errorHandler) throws Exception {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		URIBuilder uriBuilder = new URIBuilder(baseUrl).setPath(path);
		if (params != null) {
			Iterator<Map.Entry<String, String>> it = params.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, String> pair = it.next();
				uriBuilder.setParameter(pair.getKey(), pair.getValue());
			}
		}
		URI uri = uriBuilder.build();
		HttpGet httpGet = new HttpGet(uri);
		CloseableHttpResponse response = httpclient.execute(httpGet);
		int code = response.getStatusLine().getStatusCode();
		boolean ok = (code == HttpStatus.SC_OK);
		if (!ok) {
			// use specific ErrorHandler in this case
			handler = errorHandler;
		}
		try {
			// handle response
			HttpEntity entity = response.getEntity();
			handler.handleReponse(entity.getContent());
			EntityUtils.consume(entity);
			return ok;
		} finally {
			response.close();
		}
	}
	
}
